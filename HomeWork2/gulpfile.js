const { src, dest, watch, parallel, series } = require('gulp');

const scss = require('gulp-sass')(require('sass'));
const browserSync = require('browser-sync');
const minifyjs = require('gulp-js-minify');
const uglify = require('gulp-uglify-es').default;
const cleanCSS = require('gulp-clean-css');
const clean = require('gulp-clean');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');
const del = require('del');


/* Paths */
const srcPath = 'src/';
const distPath = 'dist/';
const paths = {
    styles: {
      src: `${srcPath}scss/**/*.scss`,
      dest: `${distPath}styles/`
    },
    scripts: {
      src: `${srcPath}js/**/*.js`,
      dest: `${distPath}scripts/`
    },
    html:{
      src: `${srcPath}index.html`,
      dest: distPath
    },
    img:{
      src:`${srcPath}img/**/*.*`,
      dest: `${distPath}img/`
    }
};

function styles() {
    return src(paths.styles.src)
        .pipe(scss({ outputStyle: 'compressed' }))
        .pipe(concat('style.min.css'))
        .pipe(autoprefixer())
        .pipe(cleanCSS())
        .pipe(dest(paths.styles.dest))
        .pipe(browserSync.stream())
}
 
function scripts() {
    return src([
        // 'node_modules/jquery/dist/jquery.js',
        paths.scripts.src
    ])
    .pipe(concat('app.min.js'))
    .pipe(minifyjs())
    .pipe(uglify())
    .pipe(dest(paths.scripts.dest))
    .pipe(browserSync.stream())
}

function html() {
    return src(paths.html.src)
    .pipe(dest(paths.html.dest))
    .pipe(browserSync.stream())
}

function watching() {
    // watch(['src/scss/**/*.scss'], styles);
    watch([paths.styles.src], styles)
    // watch(['src/js/app.js'], scripts);
    watch([paths.scripts.src], scripts);
    // watch(['src/*.html']).on('change', browserSync.reload)
    watch([paths.html.src]).on('change', browserSync.reload)
}

function reloadPage() {
    browserSync.init({
        server: {
            baseDir: distPath,
            port: 3000,
            keepLive: true,
        }
    })
}

function imageMin() {
    return src(paths.img.src)
    // .pipe(imagemin([
    //     imagemin.gifsicle({interlaced: true}),
    //     imagemin.mozjpeg({quality: 75, progressive: true}),
    //     imagemin.optipng({optimizationLevel: 5}),
    //     imagemin.svgo({
    //         plugins: [
    //             {removeViewBox: true},
    //             {cleanupIDs: false}
    //         ]
    //     })
    // ]))
    .pipe(dest(paths.img.dest))
}

// function cleanDist() {
//     return src(distPath)
//     .pipe(clean())
// }

function clearDist() {
    return del(distPath)
}

// exports.styles = styles;
// exports.scripts = scripts;
// exports.html = html;
// exports.watching = watching;
// exports.reloadPage = reloadPage;
// exports.imageMin = imageMin;
// exports.clearDist = clearDist;
// exports.cleanDist = cleanDist;

exports.build = series(clearDist, parallel(html, scripts, imageMin, styles));
exports.dev = parallel(watching, reloadPage);
